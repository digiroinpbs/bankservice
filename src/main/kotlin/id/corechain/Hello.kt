package id.corechain

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mashape.unirest.http.Unirest
import spark.Spark.*
import java.text.SimpleDateFormat
import java.util.*

fun main(args: Array<String>) {
    port(7067)

    val cid = "00007" // from BNI
    val key = "2c4986a0f96624b2a6f668b9169e4dbf" // from BNI
    post("/digiroin/callback/va/bni") {req, res ->
        var body:String = req.body()
        val json: JsonObject
        val parser: JsonParser = JsonParser()

        try{
            json = parser.parse(body) as JsonObject
            var clientId= json.get("client_id")
            var data= json.get("data")
            if(clientId==null || data==null){
                res.status(400)
                return@post "{ \"status\":\"error\",\"message\":\"invalid parameter\"}"
            }
            var body:String = req.body()
            val json: JsonObject
            val parser: JsonParser = JsonParser()
            try{
                json = parser.parse(body) as JsonObject
                var data= json.get("data").asString
                var clientId= json.get("client_id").asString
                var value =BniEncryption.parseData(data,cid,key)
                val jsonObject = parser.parse(""+value+"").asJsonObject
                var va= jsonObject.get("virtual_account").asString
                var paymentAmount= jsonObject.get("payment_amount").asString
                var datetimePayment= jsonObject.get("datetime_payment").asString
                val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val date = df.parse(datetimePayment)
                val epoch = date.time
                val param = "{\"AccountNumber\":\"$va\",\"Amount\":\"$paymentAmount\",\"Cashtag\":\"\$bni\"}"
                val jsonResponse = Unirest.post("http://128.199.158.155:7066/api/digiroin/service/va/cashin").header("Content-Type","application/json")
                        .body(param).asJson()
                print(jsonResponse.body)
                return@post "{ \"status\":\"000\"}"
            }catch (e:Exception){
                print(e.printStackTrace())
                e.stackTrace
                res.status(500)
                return@post "{ \"status\":\"error\",\"message\":\""+e.message+"\"}"
            }
        }catch (e:Exception){
            res.status(500)
            return@post "{ \"status\":\"error\",\"message\":\""+e.message+"\"}"
        }
        res.status(500)
        return@post "{ \"status\":\"error\",\"message\":\"unprocess\"}"
    }

    post("/digiroin/create/va/bni") {req, res ->
        var body:String = req.body()
        val json: JsonObject
        val parser: JsonParser = JsonParser()
        val now = Date()
        val time = now.time

        try{
            json = parser.parse(body) as JsonObject
            try{
                var type= "createbilling"
                var clientId= "00007"
                var customerName=json.get("CustomerName").asString
                var virtualAccount=""
                if(json.get("VirtualAccount")!=null){
                    virtualAccount=json.get("VirtualAccount").asString
                }
                var billingType= "o"
                val data ="{\"type\":\"$type\",\"client_id\":\"$clientId\",\"trx_id\":\"$time\",\"trx_amount\":\"0\",\"billing_type\":\"$billingType\",\"customer_name\":\"$customerName\",\"virtual_account\":\"$virtualAccount\"}"
                val hash = BniEncryption.hashData(data,cid,key)
                val param = "{\"client_id\":\"$clientId\",\"data\":\"$hash\"}"
                val jsonResponse = Unirest.post("https://apibeta.bni-ecollection.com").header("Content-Type","application/json")
                        .body(param).asJson()
                try{
                    val myObj = jsonResponse.getBody().getObject()
                    val status = myObj.getString("status")
                    if(status=="000"){
                        val msg = myObj.getString("data")
                        return@post BniEncryption.parseData(msg,cid,key)
                    }else{
                        res.status(500)
                        val msg = myObj.getString("message")
                        return@post "{ \"status\":\""+status+"\",\"message\":\""+msg+"\"}"
                    }
                }catch (e:Exception){
                    res.status(500)
                    return@post "{ \"status\":\"error\",\"message\":\""+e.message+"\"}"
                }
            }catch (e:Exception){
                res.status(400)
                return@post "{ \"status\":\"error\",\"message\":\"invalid parameter\"}"
            }
        }catch (e:Exception){
            res.status(500)
            return@post "{ \"status\":\"error\",\"message\":\""+e.message+"\"}"
        }
    }

}
